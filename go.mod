module gitlab.com/heyitscassio/deezgo

go 1.16

require (
	github.com/bogem/id3v2 v1.2.0
	github.com/go-flac/flacpicture v0.2.0
	github.com/go-flac/flacvorbis v0.1.0
	github.com/go-flac/go-flac v0.3.1
	github.com/godeezer/lib v1.2.1
)
