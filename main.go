package main

import (
	"bufio"
	"encoding/json"
	"errors"
	"flag"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"text/tabwriter"
	"time"

	"github.com/bogem/id3v2"
	"github.com/go-flac/flacpicture"
	"github.com/go-flac/flacvorbis"
	"github.com/go-flac/go-flac"
	"github.com/godeezer/lib/deezer"
)

type Config struct {
	Arl  string `json:"arl"`
	Path string `json:"defaultpath"`
}

type Downloader struct {
	content deezer.ContentType
	client  deezer.Client
	path    string
	format  deezer.Quality
}

type Search struct {
	Album  *bool
	Artist *bool
	Track  *bool
	Querry string
}

func (config *Config) init() error {
	var err error

	configDir, _ := os.UserConfigDir()
	configDir = filepath.Join(configDir, "deezgo")
	configFile := filepath.Join(configDir, "config.json")

	if _, err := os.Stat(configFile); os.IsNotExist(err) {
		reader := bufio.NewReader(os.Stdin)

		fmt.Print("Paste here your arl: ")
		arl, err := reader.ReadString('\n')
		if err != nil {
			return err
		}

		config := Config{
			Path: ".",
			Arl:  strings.TrimSuffix(arl, "\n"),
		}

		file, err := json.MarshalIndent(config, "", "\t")
		if err != nil {
			return err
		}

		err = os.MkdirAll(configDir, os.ModePerm)
		if err != nil {
			return err
		}

		ioutil.WriteFile(configFile, file, os.ModePerm)
		log.Println("The default config file has been written to " + configFile)
	}

	data, err := ioutil.ReadFile(configFile)
	if err != nil {
		return err
	}

	err = json.Unmarshal(data, &config)
	if err != nil {
		log.Fatalln(err)
	}

	return err
}

func tagMp3(song deezer.Song, file string) error {
	var artistStr string

	mp3file, err := id3v2.Open(file, id3v2.Options{Parse: true})
	if err != nil {
		log.Fatalln(err)
	}
	defer mp3file.Close()

	cover, err := ioutil.ReadFile(downloadCover(song))
	if err != nil {
		log.Fatalln(err)
	}

	log.Println("Tagging: " + filepath.Base(file))

	// ID3 v2
	pic := id3v2.PictureFrame{
		Encoding:    id3v2.EncodingUTF8,
		MimeType:    "image/jpeg",
		PictureType: id3v2.PTFrontCover,
		Description: "cover",
		Picture:     cover,
	}

	artistStr = song.Artists[0].Name

	for i := 1; i < len(song.Artists); i++ {
		artistStr = artistStr + "/" + song.Artists[i].Name
	}

	mp3file.AddTextFrame(mp3file.CommonID("Track number/Position in set"),
		mp3file.DefaultEncoding(),
		fmt.Sprintf("%d", song.TrackNumber))

	mp3file.AddTextFrame(mp3file.CommonID("Band/Orchestra/Accompaniment"),
		mp3file.DefaultEncoding(),
		song.ArtistName)

	mp3file.AddAttachedPicture(pic)

	mp3file.SetAlbum(song.AlbumTitle)
	mp3file.SetArtist(artistStr)
	mp3file.SetTitle(song.Title)
	mp3file.SetYear(song.DigitalReleaseDate)

	err = mp3file.Save()
	if err != nil {
		log.Fatalln(err)
	}

	return err
}

func extractFLACComment(fileName string) (*flacvorbis.MetaDataBlockVorbisComment, int) {
	f, err := flac.ParseFile(fileName)
	if err != nil {
		panic(err)
	}

	var cmt *flacvorbis.MetaDataBlockVorbisComment
	var cmtIdx int
	for idx, meta := range f.Meta {
		if meta.Type == flac.VorbisComment {
			cmt, err = flacvorbis.ParseFromMetaDataBlock(*meta)
			cmtIdx = idx
			if err != nil {
				panic(err)
			}
		}
	}
	return cmt, cmtIdx
}

func tagFlac(song deezer.Song, file string) error {
	f, err := flac.ParseFile(file)
	if err != nil {
		panic(err)
	}

	imgData, err := ioutil.ReadFile(downloadCover(song))
	picture, err := flacpicture.NewFromImageData(flacpicture.PictureTypeFrontCover, "Front cover", imgData, "image/jpeg")
	if err != nil {
		panic(err)
	}

	log.Println("Tagging: " + filepath.Base(file))

	picturemeta := picture.Marshal()
	f.Meta = append(f.Meta, &picturemeta)

	cmts, idx := extractFLACComment(file)
	if cmts == nil && idx > 0 {
		cmts = flacvorbis.New()
	}
	// Clear vorbis comments
	for i := range cmts.Comments {
		cmts.Comments[i] = "\000"
	}

	for i := range song.Artists {
		cmts.Add(flacvorbis.FIELD_ARTIST, song.Artists[i].Name)
	}

	cmts.Add(flacvorbis.FIELD_TITLE, song.Title)
	cmts.Add("ALBUMARTIST", song.ArtistName)
	cmts.Add(flacvorbis.FIELD_ALBUM, song.AlbumTitle)
	cmts.Add(flacvorbis.FIELD_TRACKNUMBER, strconv.Itoa(song.TrackNumber))
	cmts.Add(flacvorbis.FIELD_DATE, song.DigitalReleaseDate)

	cmtsmeta := cmts.Marshal()
	if idx > 0 {
		f.Meta[idx] = &cmtsmeta
	} else {
		f.Meta = append(f.Meta, &cmtsmeta)
	}
	f.Save(file)
	return err
}

func downloadCover(song deezer.Song) string {
	tmppath := "/tmp/deezgo-covers"
	link := "https://cdns-images.dzcdn.net/images/cover/" + song.AlbumPicture + "/500x500-000000-80-0-0.jpg"
	filename := filepath.Join(tmppath, "cover"+song.AlbumID+".jpg")

	if _, err := os.Stat(filename); err == nil {
		return filename
	}

	err := os.MkdirAll(tmppath, os.ModePerm)
	if err != nil {
		log.Fatalln(err)
	}

	log.Println("downloading cover:" + filename)

	// Create blank file
	file, err := os.Create(filename)
	if err != nil {
		log.Fatal(err)
	}
	client := http.Client{
		CheckRedirect: func(r *http.Request, via []*http.Request) error {
			r.URL.Opaque = r.URL.Path
			return nil
		},
	}

	// Put content on file
	resp, err := client.Get(link)
	if err != nil {
		log.Fatal(err)
	}
	defer resp.Body.Close()

	_, err = io.Copy(file, resp.Body)

	defer file.Close()

	return filename
}

func sanitize(filename string) string {
	return strings.ReplaceAll(filename, "/", "_")
}

func newDownloader(client deezer.Client, content deezer.ContentType, format deezer.Quality) *Downloader {
	var downloader Downloader

	downloader.client = client
	downloader.content = content
	downloader.format = format

	return &downloader
}

func (downloader *Downloader) DownloadTrack(id string, path string) error {
	var ext string

	song, err := downloader.client.Song(id)
	if err != nil {
		log.Println(err)
	}

	if !downloader.client.IsQualityAvailable(*song, downloader.format) {
		switch downloader.format {
		case deezer.FLAC:
			log.Println("Format not avalible, falling back to mp3320")
			downloader.format = deezer.MP3320
			return err
		case deezer.MP3320:
			log.Println("Format not avalible, falling back to mp3128")
			downloader.format = deezer.MP3128
			return err
		case deezer.MP3128:
			log.Println("Format not avalible, falling back to flac")
			downloader.format = deezer.FLAC
			err = downloader.DownloadTrack(id, path)
			return err
		}
	}

	if downloader.format == deezer.FLAC {
		ext = ".flac"
	} else {
		ext = ".mp3"
	}

	filename := sanitize(fmt.Sprintf("%.2d - %s%s", song.TrackNumber, song.Title, ext))

	err = os.MkdirAll(path, os.ModePerm)
	if err != nil {
		log.Fatalln(err)
	}

	pathToFile := filepath.Join(path, filename)

	outFile, err := os.Create(pathToFile)
	if err != nil {
		log.Fatalln(err)
	}

	log.Println("Downloading track: " + filename)

	err = song.Write(outFile, downloader.format)
	if err != nil {
		log.Println(err)
	}
	defer outFile.Close()

	if downloader.format != deezer.FLAC {
		err = tagMp3(*song, pathToFile)
	} else {
		err = tagFlac(*song, pathToFile)
	}

	return err
}

func (downloader *Downloader) DownloadAlbum(id string, path string) error {
	songs, err := downloader.client.SongsByAlbum(id, -1)
	if err != nil {
		log.Fatalln(err)
	}

	log.Println("Downloading from Album: " + songs[0].AlbumTitle)

	dir := sanitize(fmt.Sprintf("%s - %s", songs[0].ArtistName, songs[0].AlbumTitle))

	c := make(chan struct{})
	for i := range songs {
		go func() {
			downloader.DownloadTrack(songs[i].ID, filepath.Join(path, dir))
			c <- struct{}{}
		}()

		time.Sleep(time.Second * 1)
	}

	for range songs {
		<-c
	}

	log.Println("Copying cover to Album directory")
	cover, err := os.Create(filepath.Join(path, dir, "cover.jpg"))
	if err != nil {
		log.Fatalln(err)
	}

	/* Taking the cover of the first song might be wrong.
	 * One album could have more then one cover? */
	tmpcover, err := os.Open(downloadCover(songs[0]))
	if err != nil {
		log.Println(err)
	}

	_, err = io.Copy(cover, tmpcover)
	if err != nil {
		log.Println(err)
	}

	return err
}

func (downloader *Downloader) DownloadArtist(id string, path string) error {
	albuns, err := downloader.client.AlbumsByArtist(id)
	if err != nil {
		log.Fatalln(err)
	}

	mainArtist := albuns[0].ArtistName

	log.Println("Downloading from Artist: " + mainArtist)

	for i := range albuns {
		if albuns[i].ArtistName == mainArtist {
			dir := albuns[i].ArtistName
			downloader.DownloadAlbum(albuns[i].ID, filepath.Join(path, dir))
		}
	}

	return err
}

func (downloader *Downloader) ContentHandler(id string, path string) error {
	var err error

	switch downloader.content {
	case "track":
		err = downloader.DownloadTrack(id, path)
		break
	case "album":
		err = downloader.DownloadAlbum(id, path)
		break
	case "artist":
		err = downloader.DownloadArtist(id, path)
		break
	default:
		err = errors.New("Unkown content type")
		break
	}

	return err
}

func (search *Search) Search(client deezer.Client) error {
	res, err := client.Search(search.Querry, "", "", 0, -1)

	if !*search.Album && !*search.Artist && !*search.Track {
		*search.Album = true
	}

	if !((*search.Album != *search.Artist) != *search.Track) {
		return errors.New("You can only search by artist, track or album one at the time")
	}

	w := new(tabwriter.Writer)

	w.Init(os.Stdout, 8, 8, 1, ' ', 0)

	defer w.Flush()

	if *search.Album {
		for i := range res.Albums.Data {
			fmt.Fprintf(w, "%s\t%s\thttps://www.deezer.com/us/album/%s\n",
				res.Albums.Data[i].Title,
				res.Albums.Data[i].ArtistName,
				res.Albums.Data[i].ID)
		}
	}

	if *search.Artist {
		for i := range res.Artists.Data {
			fmt.Fprintf(w, "%s\thttps://www.deezer.com/us/artist/%s\n",
				res.Artists.Data[i].Name,
				res.Artists.Data[i].ID)
		}
	}

	if *search.Track {
		for i := range res.Songs.Data {
			fmt.Fprintf(w, "%s\t%s\thttps://www.deezer.com/us/track/%s\n",
				res.Songs.Data[i].Title,
				res.Songs.Data[i].ArtistName,
				res.Songs.Data[i].ID)
		}
	}

	return err
}

func main() {
	config := new(Config)

	err := config.init()
	if err != nil {
		log.Fatalln(err)
	}

	if len(os.Args) < 2 {
		log.Fatalln("expected get, search commands")
	}

	client := deezer.NewClient(config.Arl)

	switch os.Args[1] {
	case "get":
		var format deezer.Quality
		var defaultPath string

		if config.Path != "" {
			defaultPath = config.Path
		} else {
			defaultPath = "."
		}

		getCmd := flag.NewFlagSet("get", flag.ExitOnError)
		path := getCmd.String("p", defaultPath, "file path")
		formatStr := getCmd.String("f", "mp3320", "format")

		getCmd.Parse(os.Args[2:])

		content, id := deezer.ParseURL(getCmd.Args()[0])

		switch *formatStr {
		case "flac":
			format = deezer.FLAC
			break
		case "mp3320":
			format = deezer.MP3320
			break
		case "mp3128":
			format = deezer.MP3128
			break
		case "mp3":
			format = deezer.MP3320
			break
		default:
			log.Fatalln("Invalid download format")
		}

		downloader := newDownloader(*client, content, format)

		err := downloader.ContentHandler(id, *path)
		if err != nil {
			log.Fatalln(err)
		}

		log.Println("Done!")
		break
	case "search":
		seachCmd := flag.NewFlagSet("seach", flag.ExitOnError)

		search := new(Search)
		search.Album = seachCmd.Bool("a", false, "search by album")
		search.Artist = seachCmd.Bool("A", false, "search by artist")
		search.Track = seachCmd.Bool("t", false, "search by track")

		seachCmd.Parse(os.Args[2:])

		search.Querry = strings.Join(seachCmd.Args(), " ")

		err := search.Search(*client)
		if err != nil {
			log.Fatalln(err)
		}

		break
	default:
		log.Fatalln("expected get, search commands")
	}
}
